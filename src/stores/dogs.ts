import { defineStore } from 'pinia'
import { api } from 'boot/axios'
import { Fact } from 'src/models/Fact'

interface FactRequestPayload {
  fact: {
    value: string
  }
}

const useDogsStore = defineStore('dogs', {
  state: () => ({
    facts: [] as Fact[],
    fact: {} as Fact,
    currentPage: 1
  }),

  actions: {
    getAllFacts (payload?: object) {
      return new Promise(resolve => {
        if (!payload) {
          payload = {}
        }

        api.get('/v1/dog-facts', {
          params: payload
        })
          .then(resp => {
            this.facts = this.facts.concat(resp.data.data)
            resolve(resp)
          })
      })
    },

    storeFact (payload: FactRequestPayload) {
      return new Promise(resolve => {
        api.post('/v1/dog-facts', { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            this.facts.push(this.fact)
          })
      })
    },

    updateFact (id: number, payload: FactRequestPayload) {
      console.log('update', id)
      return new Promise(resolve => {
        api.put(`/v1/dog-facts/${id}`, { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            const index = this.facts.findIndex(i => i.id === id)
            this.facts[index] = this.fact
          })
      })
    },

    deleteFact (id: number) {
      return new Promise(() => {
        api.delete(`/v1/dog-facts/${id}`)
          .then(() => {
            const index = this.facts.findIndex(i => i.id === id)
            this.facts.splice(index, 1)
          })
      })
    }
  }
})

export default useDogsStore
