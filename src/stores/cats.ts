import { defineStore } from 'pinia'
import { api } from 'boot/axios'
import { Fact } from 'src/models/Fact'

interface FactRequestPayload {
  fact: {
    value: string
  }
}

const useCatsStore = defineStore('cats', {
  state: () => ({
    facts: [] as Fact[],
    fact: {} as Fact
  }),

  actions: {
    getAllFacts (payload?: object) {
      return new Promise(resolve => {
        if (!payload) {
          payload = {}
        }

        api.get('/v1/cat-facts', {
          params: payload
        })
          .then(resp => {
            this.facts = this.facts.concat(resp.data.data)
            resolve(resp)
          })
      })
    },

    storeFact (payload: FactRequestPayload) {
      return new Promise(resolve => {
        api.post('/v1/cat-facts', { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            this.facts.push(this.fact)
          })
      })
    },

    updateFact (id: number, payload: FactRequestPayload) {
      console.log('update', id)
      return new Promise(resolve => {
        api.put(`/v1/cat-facts/${id}`, { fact: payload.fact.value })
          .then(resp => {
            this.fact = resp.data
            const index = this.facts.findIndex(i => i.id === id)
            this.facts[index] = this.fact
          })
      })
    },

    deleteFact (id: number) {
      return new Promise(() => {
        api.delete(`/v1/cat-facts/${id}`)
          .then(() => {
            const index = this.facts.findIndex(i => i.id === id)
            this.facts.splice(index, 1)
          })
      })
    }
  }
})

export default useCatsStore
